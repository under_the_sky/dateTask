/**
* 为了不让该对象的错误影响系统，这里进行一下闭包操作
*/
var KGODate = (function(){
	var _selected_style = 'kgo-date-selected';
	var _today_style = 'kgo-date-today';
	var _tasks = '_taskJson';
	/**
	 * 内部方法，获取当前月共有多少天
	 * 打印上个月的最后几天时需要知道上个月共有几天
	 * 
	 */
	function getMonthTotalDays(year , month){
		//当前月的开始日期
		var startDate = new Date(year,month-1,1);
		//当前月的结束日期
		var endDate = new Date(year,month,0); 
		//当前月共多少天
		var gapDay = Math.abs(endDate - startDate) / (1000*60*60*24) + 1 ;
		return gapDay;
	}
	/**
	 * 定义一个内部方法获取当月的日期数据，
	 */
	function getMonthDays(year,month){
			//gapDay 标示这个月和上个月中间相差多少天，意思就是当月共有多少天 
			var today , thisMonth ,thisYear ,thisfirstDay ,thisLastDay , gapDay;
			var dateArray = [];
			//如果没有传年或者月，就认为是当前月 
			if(!year||!month){
				today = new Date;
				thisMonth = today.getMonth()+1;
				thisYear = today.getFullYear();
				var startDate = new Date(thisYear,thisMonth-1,1);
				var endDate = new Date(thisYear,thisMonth,0);
				thisfirstDay =  startDate.getDay();
				thisLastDay =  endDate.getDay();
				//一星期有7天，外国星期天是第一天，所以星期天是 0 这些做一下转化
				//if(thisLastDay==0) thisLastDay = 7;
				thisLastDay = thisLastDay==0 ? 7 : thisLastDay ;
				gapDay = Math.abs(endDate - startDate) / (1000*60*60*24) + 1 ;
			}else{
				//获取当前月，js的月份是从0-11的，所以传入数据需要 -1 
				today = new Date(year,month-1);
				//年数据，月数据有可能发生越界，比如 11月31号 是12月1号 ，这里需要重新获取一些当前月和当前年
				thisMonth = today.getMonth()+1;
				thisYear = today.getFullYear();
				var startDate = new Date(thisYear,thisMonth-1,1);
				var endDate = new Date(thisYear,thisMonth,0);
				//这里的firstDay 是本月的第一天是周几
				thisfirstDay =  startDate.getDay();
				//LastDay 同上 
				thisLastDay =  endDate.getDay();
				//一星期有7天，外国星期天是第一天，所以星期天是 0 这些做一下转化
				//if(thisLastDay==0) thisLastDay = 7;
				thisLastDay = thisLastDay==0 ? 7 : thisLastDay ;
				//日期相减返回的是毫秒，并且会少一天 
				gapDay = Math.abs(endDate - startDate) / (1000*60*60*24) + 1 ;
			
			}
			//当前数据表格需要输出多少天 ，sum = 当月天数 + 上个月最后几天 + 下个月的开始几天 
			//arraySize 一定是7的整数倍 ，但是有可能是4*7 ，有可能是5*7 也有可能是 6*7 
			//这里计算 数组长度是为了返回最小的 数组长度 不严谨的做法可以统一用 6*7
			var arraySize =  parseInt(gapDay) + parseInt(thisfirstDay) +(7-thisLastDay-1);
			//上一个月的最后一天
			var lastMonthLastDay =getMonthTotalDays(thisYear,thisMonth-1);
		
			//下面开始循环将日历的数据放数组中
			for(var i=0; i < arraySize;i++){
				// 星期几 ？ i是从零开始的，所以这里要 +1
				var _weeknum = (i+1)%7;
				//如果是周天，对7取余数会等 0 ，这里三目运算修改一下
				_weeknum = _weeknum==0 ? 7 : _weeknum ;
				// 打印上个月的最后几天，这里本月的第一天是从1开始计算的（周天被转化为了7）所以要打印到前一天
				if(i<thisfirstDay-1){
					dateArray.push({
						week:_weeknum,
						type:'lastMonth',
						days:lastMonthLastDay-(thisfirstDay-1)+i+1,
					});
				//打印当月时间
				}else if(Math.abs(i+1-thisfirstDay+1) <= gapDay){
					dateArray.push({
						week:_weeknum,
						type:'thisMonth',
						days:Math.abs(i+1-thisfirstDay+1),
					});
				//打印下个月数据
				}else{
					dateArray.push({
						week:_weeknum,
						type:'nextMonth',
						days:Math.abs(i+1-thisfirstDay - gapDay)+1,
					});
				}
				
			}
			//返回对象，当前年，当前月，当前月天数据
			return {
				year:thisYear,
				month:thisMonth,
				days:dateArray
			};
		} 
		/**
		 * 添加内部方法打印日历头部信息
		 * 
		 */
		function printHeadDom($dom,days){
			var _kgo_date_dom =	'<div id="kgo-date-content">'
							+'	<div class="kgo-date-task-title">'
							+ days.year +'-' + days.month
							+'  </div>'
							+'	<table class="kgo-date-task" cellspacing="0">'
							+'			<colgroup>'
							+'				<col>'
							+'				<col>'
							+'				<col>'
							+'				<col>'
							+'				<col>'
							+'				<col style="background-color: rgb(51,255,202,0.3);">'
							+'				<col style="background-color: rgb(51,255,202,0.3);">'
							+'			</colgroup>'
							+'		<thead>'
							+'		<tr>'
							+'			<th>星期一</th>'
							+'			<th>星期二</th>'
							+'			<th>星期三</th>'
							+'			<th>星期四</th>'
							+'			<th>星期五</th>'
							+'			<th>星期六</th>'
							+'			<th>星期天</th>'
							+'		</tr> '
							+'		</thead>'
							+'		<tbody class="date-day-table">'
							+'	</table>'
							+'</div>	 '
			$dom.append(_kgo_date_dom);
		};
		/**
		 * 定义内部方法打印 日期信息
		 */
		function printDateBody($dom,days){
			var dom ="";
			$.each(days.days,function(index,item){
				var td_date_sty = 'this-month-day-sty'
				if(item.type=='lastMonth'||item.type=='nextMonth'){
					var td_date_sty = 'not-this-month-day-sty'
				}
				if((index+1)%7==1){
					dom += '<tr>';
				} 
				var  theday ="";
				if(item.type=='thisMonth'){
					
					if(item.days<10){
						theday=days.year+"-"+days.month+"-0"+item.days;
					}else{
						theday=days.year+"-"+days.month+"-"+item.days;
					}
					
				}
			   
				dom  += '<td class="'+td_date_sty+'" theday="'+theday+'">'
				     + '<span class="kgo-date-day">'+item.days+'</span></td>';
				if((index+1)%7==0){
					dom+='</tr>';
				} 
			})
			$('.date-day-table').append(dom);
			// 单元选择事件
			$('.kgo-date-task  td').on('click',function(){
				 //alert($(this).text());
			});
			
		};
		/**
		 * 标记今天
		 */
		 function markToday(){
			 var myDate = new Date();
			 var _toDay = myDate.toLocaleDateString().split('/');
		
// 			 if(_toDay[1]<10){
// 				 _toDay = "0"+ _toDay
// 			 }
 			 if(_toDay[2]<10){
 				  _toDay[2] = "0"+ _toDay[2]
 			 }
			 var today_date = _toDay[0]+"-"+_toDay[1]+"-"+_toDay[2];
			console.log(_selected_style);
			$('.kgo-date-task  td[theday='+today_date+']').addClass(_today_style);
			
		 }
		/**
		 * 将任务打印到日期中
		 */
		function printDateTask(tasks){
			
		
			$.each(tasks,function(index,task){
				var dom ='<div class="kgo-date-task-body" task="'+task.date+_tasks+'">';
				sessionStorage.setItem(task.date+_tasks, JSON.stringify(task));
				$.each(task.tasks,function(index,item){
					dom +='<span class="kgo-'+item.type+'-badge kgo-badge-total">未完成（'+item.sum+'）</span><br>';
				});
				dom += 	'</div>';
				$('.kgo-date-task  td[theday='+task.date+']').append(dom);
			
			});
			
		//	var _tasks = JSON.stringify(tasks).replace(/[ ]/g,"").replace(/[\r\n]/g,"");
		};
		/**
		 * 封装点击回调方法
		 */
			function dateOnclick(callback){
				
				$('.kgo-date-task  td').on('click',function(){ 
					var click_date = $(this).attr('theday');
				
					var click_task = "";
					if($(this).find('.kgo-date-task-body').length>0){
						click_task = $(this).find('.kgo-date-task-body').attr('task');
					click_task = JSON.parse(sessionStorage.getItem(click_task));
					}
					callback({date:click_date,tasks:click_task});
				});
					
			};
		/**
		 * 暴露的属性和方法
		 */
		return {
			//初始化方法
			init : function($dom,year,month){
				var days = getMonthDays(year,month);
				printHeadDom($dom,days);
				printDateBody($dom,days);
				markToday();
			},
			//新增任务方法
			addTask: function(day){
				printDateTask(day);
			},
			// 点击触发
			onClickDo: function(callback){
				dateOnclick(callback);
			}
			
			
		}
						
		
})();